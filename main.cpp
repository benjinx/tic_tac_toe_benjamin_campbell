#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include <stdio.h>
#include "RakNetTypes.h"
#include <iostream>


// Copied from Multiplayer.cpp
// If the first byte is ID_TIMESTAMP, then we want the 5th byte
// Otherwise we want the 1st byte
unsigned char GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

const int CLIENT_PORT = 201;
const int SERVER_PORT = 200;
const char IP[64] = "127.0.0.1";

namespace tictactoe{
	enum MESSAGE{
		ID_YOUR_TURN = ID_USER_PACKET_ENUM


	};

#pragma pack(push, 1)
	struct Board
	{
		enum { EMPTY = 0, CROSS, CIRCLE }; typedef int move_type;



		move_type mBoard[9];

		void init() { for (int i = 0; i < 9; i++) mBoard[i] = EMPTY; }

		bool isEmpty(int index) { return mBoard[index] == EMPTY; }
		bool isCross(int index) { return mBoard[index] == CROSS; }
		bool isCircle(int index){ return mBoard[index] == CIRCLE; }

		move_type operator[] (int index) { return mBoard[index]; }

		bool DeLogic(Board& b)
		{
			for (int i = 0; i < 9; ++i)
			{
				if (b[i] == Board::EMPTY)
					printf("%d", i);
				else if (b[i] == Board::CIRCLE)
					printf(" O ");
				else if (b[i] == Board::CROSS)
					printf(" X ");

				if (i == 2)
					printf("\n");
			}

			int cellIndex;
			std::cin >> cellIndex;
		}
	};
#pragma pop
};


void startServer(RakNet::RakPeerInterface* server)
{
	RakNet::SocketDescriptor serverSocketDescriptor;
	server = RakNet::RakPeerInterface::GetInstance();
	server->SetIncomingPassword("test", (int)strlen("test"));
	server->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	serverSocketDescriptor.port = SERVER_PORT;
	serverSocketDescriptor.socketFamily = AF_INET;
	server->Startup(4, &serverSocketDescriptor, 1);
	server->SetOccasionalPing(true);
	server->SetUnreliableTimeout(1000);
	DataStructures::List< RakNet::RakNetSocket2* > sockets;
	server->GetSockets(sockets);

	printf("\nMy IP addresses as a server:\n");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}
}

void startClient(RakNet::RakPeerInterface* client)
{
	RakNet::SocketDescriptor clientSocketDescriptor;
	client = RakNet::RakPeerInterface::GetInstance();
	clientSocketDescriptor.port = CLIENT_PORT;
	clientSocketDescriptor.socketFamily = AF_INET;
	client->Startup(8, &clientSocketDescriptor, 1);
	client->SetOccasionalPing(true);
	client->Connect(IP, SERVER_PORT, "test", (int)strlen("test"));

	printf("\nMy IP addresses:\n");
	unsigned int i;
	for (i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, client->GetLocalIP(i));
	}
}

void runCommon(RakNet::RakPeerInterface* peer)
{
	while (1)
	{
		RakNet::SystemAddress clientID = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
		RakNet::Packet* p;
		for (p = peer->Receive(); p; peer->DeallocatePacket(p), p = peer->Receive())
		{
			// We got a packet, get the identifier with our handy function
			unsigned char packetIdentifier = GetPacketIdentifier(p);

			// Check if this is a network message packet
			switch (packetIdentifier)
			{
			case ID_ALREADY_CONNECTED:
				// Connection lost normally
				printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", p->guid);
				break;
			case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_CONNECTION_LOST\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");

				tictactoe::Board b;
				b.init();
				peer->Send((const char*)&b, sizeof(b), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_RAKNET_GUID, true);
				break;
			case ID_CONNECTION_BANNED: // Banned from this server
				printf("We are banned from this server.\n");
				break;
			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("Connection attempt failed\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				// Sorry, the server is full.  I don't do anything here but
				// A real app should tell the user
				printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				break;

			case ID_INVALID_PASSWORD:
				printf("ID_INVALID_PASSWORD\n");
				break;

			case ID_CONNECTION_REQUEST_ACCEPTED:
				// This tells the client they have connected
				printf("ID_CONNECTION_REQUEST_ACCEPTED to %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
				printf("My external address is %s\n", peer->GetExternalID(p->systemAddress).ToString(true));
				break;





			case ID_NEW_INCOMING_CONNECTION:
				// Somebody connected.  We have their IP now
				printf("ID_NEW_INCOMING_CONNECTION from %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
				clientID = p->systemAddress; // Record the player ID of the client

				printf("Remote internal IDs:\n");
				for (int index = 0; index < MAXIMUM_NUMBER_OF_INTERNAL_IDS; index++)
				{
					RakNet::SystemAddress internalId = peer->GetInternalID(p->systemAddress, index);
					if (internalId != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
					{
						printf("%i. %s\n", index + 1, internalId.ToString(true));
					}
				}

				break;




				// common
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
				break;

			case ID_DISCONNECTION_NOTIFICATION:
				// Connection lost normally
				printf("ID_DISCONNECTION_NOTIFICATION from %s\n", p->systemAddress.ToString(true));;
				break;

			case ID_CONNECTION_LOST:
				// Couldn't deliver a reliable packet - i.e. the other system was abnormally
				// terminated
				printf("ID_CONNECTION_LOST from %s\n", p->systemAddress.ToString(true));;
				break;

			case tictactoe::MESSAGE::ID_YOUR_TURN:
				printf("IT IS MY TURN!");

				//tictactoe::Board* b = reinterpret_cast<tictactoe::Board*>(p->data);
					break;

			default:
				// The server knows the static data of all clients, so we can prefix the message
				// With the name data
				printf("Default!");

				break;
			}
		}
	}
}

int main(int argc, char** argv)
{
	
	RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();
	
	if (atoi(argv[1]) == 0)
	{
		startServer(peer);
	}
	else
	{
		startClient(peer);
	}
	
	runCommon(peer);
	system("pause");
	return 0;
}
